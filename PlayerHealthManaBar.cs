﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader.IO;
using Terraria.ModLoader;
using Terraria;

namespace PlayerHealthManaBar {
	public class PlayerHealthManaBar : Mod {
		public PlayerHealthManaBar() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}
		
		internal static bool LoadedFKTModSettings = false;
		internal static bool PowerAttacksLoaded = false;
		
		public override void Load() {
			Config.ReadConfig();
			if (Main.netMode == 2) { return; }
			Textures.BigHealthBar1 = GetTexture("Images/BigHealthBar1");
			Textures.BigHealthBar2 = GetTexture("Images/BigHealthBar2");
			Textures.BigHeart = GetTexture("Images/Heart");
			Textures.Bubble = GetTexture("Images/Bubble");
			Textures.Exclimation = GetTexture("Images/Exclimation");
			Textures.Flame = GetTexture("Images/Flame");
			Textures.HealthBar1 = GetTexture("Images/HealthBar1");
			Textures.HealthBar2 = GetTexture("Images/HealthBar2");
			Textures.Heart = GetTexture("Images/Heart");
			Textures.Mana = GetTexture("Images/Mana");
			Textures.ManaSick = GetTexture("Images/ManaSick");
			Textures.PotionSick = GetTexture("Images/PotionSick");

			Textures.PA_Critical = GetTexture("Images/PA_Critical");
			Textures.PA_Damage = GetTexture("Images/PA_Damage");
			Textures.PA_FullCharge = GetTexture("Images/PA_FullCharge");
			Textures.PA_Power = GetTexture("Images/PA_Power");
			Textures.PA_Velocity = GetTexture("Images/PA_Velocity");

			On.Terraria.Main.DrawInterface_14_EntityHealthBars += UI.DrawHealthBar;
			On.Terraria.Main.DrawInterface_Resources_Breath += (DrawInterface_Resources_Breath) => {
				if (!Config.Client.ShowPlayerOtherResourceBars) { DrawInterface_Resources_Breath(); }
				return;
			};
			
			PowerAttacksLoaded = ModLoader.GetMod("PowerAttacks") != null;
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddBool("ShowPlayerHealthBars", "Show Health Bar", false);
			setting.AddBool("ShowPlayerManaBars", "Show Mana Bar", false);
			setting.AddBool("ShowPlayerOtherResourceBars", "Show Other Resource Bars", false);
			setting.AddBool("ShowPotionSicknessBars", "Show Potion Sickness Bars", false);
			setting.AddBool("ShowNPCHealthBars", "Show NPC Health Bars", false);
			setting.AddBool("ShowIcons", "Show Icons", false);
			setting.AddBool("ShowWarningIcons", "Show Warning Icons", false);
			setting.AddFloat("WarningThreshold", "Warning Threshold", 0, 1, false);
			setting.AddFloat("FadeInAmount", "Fade In Amount", 0, 1, false);
			setting.AddFloat("FadeOutAmount", "Fade Out Amount", 0, 1, false);
			setting.AddFloat("MaxAlpha", "Max Alpha", 0, 1, false);
			try { setting.AddInt("BarSpacing", "Bar Spacing", -(Textures.HealthBar1.Height / 2), (Textures.HealthBar1.Height / 2), false); } catch {}
//			if (PowerAttacksLoaded) {
//				setting.AddComment("Power Attacks", 0.8f);
//				setting.AddBool("PA_ShowChargeBar", "Show Charge Bar", false);
//			}
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting))  {
				setting.Get("ShowPlayerHealthBars", ref Config.Client.ShowPlayerHealthBars);
				setting.Get("ShowPlayerManaBars", ref Config.Client.ShowPlayerManaBars);
				setting.Get("ShowPlayerOtherResourceBars", ref Config.Client.ShowPlayerOtherResourceBars);
				setting.Get("ShowPotionSicknessBars", ref Config.Client.ShowPotionSicknessBars);
				setting.Get("ShowNPCHealthBars", ref Config.Client.ShowNPCHealthBars);
				setting.Get("ShowIcons", ref Config.Client.ShowIcons);
				setting.Get("ShowWarningIcons", ref Config.Client.ShowWarningIcons);
				setting.Get("WarningThreshold", ref Config.Client.WarningThreshold);
				setting.Get("FadeInAmount", ref Config.Client.FadeInAmount);
				setting.Get("FadeOutAmount", ref Config.Client.FadeOutAmount);
				setting.Get("MaxAlpha", ref Config.Client.MaxAlpha);
				setting.Get("BarSpacing", ref Config.Client.BarSpacing);
//				if (PowerAttacksLoaded) {
//					setting.Get("PA_ShowChargeBar", ref PowerAttacks.Config.Client.ShowChargeBar);
//				}
			}
		}
		
		public override void HandlePacket(BinaryReader reader, int whoAmI) {
			if (reader.ReadString() != Config.modPrefix) { return; }
			Player player = Main.player[reader.ReadInt32()];
			if (player.active) {
				player.breath = reader.ReadInt32();
				player.breathMax = reader.ReadInt32();
				player.lavaTime = reader.ReadInt32();
				player.lavaMax = reader.ReadInt32();
				if (Main.netMode == 2) {
					for (int i = 0; i < (int)Byte.MaxValue; i++) {
						Player otherPlayer = Main.player[i];
						if (otherPlayer.active && otherPlayer.whoAmI != player.whoAmI) {
							ModPacket netMessage = this.GetPacket();
							netMessage.Write(Config.modPrefix);
							netMessage.Write((int)player.whoAmI);
							netMessage.Write((int)player.breath);
							netMessage.Write((int)player.breathMax);
							netMessage.Write((int)player.lavaTime);
							netMessage.Write((int)player.lavaMax);
							netMessage.Send(i);
						}
					}
				}
			}
		}
	}
	
	public static class Textures {
		public static Texture2D BigHealthBar1;
		public static Texture2D BigHealthBar2;
		public static Texture2D BigHeart;
		public static Texture2D Bubble;
		public static Texture2D Exclimation;
		public static Texture2D Flame;
		public static Texture2D HealthBar1;
		public static Texture2D HealthBar2;
		public static Texture2D Heart;
		public static Texture2D Mana;
		public static Texture2D ManaSick;
		public static Texture2D PotionSick;
		
		public static Texture2D PA_Critical;
		public static Texture2D PA_Damage;
		public static Texture2D PA_FullCharge;
		public static Texture2D PA_Power;
		public static Texture2D PA_Velocity;
	}
	
	public static class Config {
		public static class Global {}
		public static class Client {
			public static bool ShowPlayerHealthBars = true;
			public static bool ShowPlayerManaBars = true;
			public static bool ShowPlayerOtherResourceBars = true;
			public static bool ShowPotionSicknessBars = true;
			public static bool ShowNPCHealthBars = true;
			public static bool ShowIcons = true;
			public static bool ShowWarningIcons = true;
			public static float WarningThreshold = 0.25f;
			public static float FadeInAmount = 0.1f;
			public static float FadeOutAmount = 0.05f;
			public static float MaxAlpha = 1f;
			public static int BarSpacing = 0;
		}
		public static class Server {}
		public static string modName = "PlayerResourceBars";
		public static string modPrefix = "PRB";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));
		
		public static void ReadConfig() {
			if(Configuration.Load()) {
				if (Main.netMode != 2) {
					Configuration.Get("ShowPlayerHealthBars", ref Client.ShowPlayerHealthBars);
					Configuration.Get("ShowPlayerManaBars", ref Client.ShowPlayerManaBars);
					Configuration.Get("ShowPlayerOtherResourceBars", ref Client.ShowPlayerOtherResourceBars);
					Configuration.Get("ShowPotionSicknessBars", ref Client.ShowPotionSicknessBars);
					Configuration.Get("ShowNPCHealthBars", ref Client.ShowNPCHealthBars);
					Configuration.Get("ShowIcons", ref Client.ShowIcons);
					Configuration.Get("ShowWarningIcons", ref Client.ShowWarningIcons);
					Configuration.Get("WarningThreshold", ref Client.WarningThreshold);
					Configuration.Get("FadeInAmount", ref Client.FadeInAmount);
					Configuration.Get("FadeOutAmount", ref Client.FadeOutAmount);
					Configuration.Get("MaxAlpha", ref Client.MaxAlpha);
					Configuration.Get("BarSpacing", ref Client.BarSpacing);
				}
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Client.WarningThreshold = (float)MathHelper.Clamp(Client.WarningThreshold, 0f, 1f);
			Client.FadeInAmount = (float)MathHelper.Clamp(Client.FadeInAmount, 0f, 1f);
			Client.FadeOutAmount = (float)MathHelper.Clamp(Client.FadeOutAmount, 0f, 1f);
			Client.MaxAlpha = (float)MathHelper.Clamp(Client.MaxAlpha, 0f, 1f);
			try { if (Textures.HealthBar1 != null) { Client.BarSpacing = (int)MathHelper.Clamp(Client.BarSpacing, -(Textures.HealthBar1.Height / 2), (Textures.HealthBar1.Height / 2)); } } catch {}
		}

		public static void SaveConfig() {
			ClampConfig();
			if (Main.netMode != 1) { Configuration.Clear(); }
			if (Main.netMode != 2) {
				Configuration.Put("ShowPlayerHealthBars", Client.ShowPlayerHealthBars);
				Configuration.Put("ShowPlayerManaBars", Client.ShowPlayerManaBars);
				Configuration.Put("ShowPlayerOtherResourceBars", Client.ShowPlayerOtherResourceBars);
				Configuration.Put("ShowPotionSicknessBars", Client.ShowPotionSicknessBars);
				Configuration.Put("ShowNPCHealthBars", Client.ShowNPCHealthBars);
				Configuration.Put("ShowIcons", Client.ShowIcons);
				Configuration.Put("ShowWarningIcons", Client.ShowWarningIcons);
				Configuration.Put("WarningThreshold", Client.WarningThreshold);
				Configuration.Put("FadeInAmount", Client.FadeInAmount);
				Configuration.Put("FadeOutAmount", Client.FadeOutAmount);
				Configuration.Put("MaxAlpha", Client.MaxAlpha);
				Configuration.Put("BarSpacing", Client.BarSpacing);
			}
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}
	}
	
	public static class UI {
		public static float[] NPCHBarAlpha = new float[200];
		public static float[] HBarAlpha = new float[255];
		public static float[] MBarAlpha = new float[255];
		public static float[] BBarAlpha = new float[255];
		public static float[] LBarAlpha = new float[255];
		public static float[] BarOffset = new float[255];
		public static float[] PSBarAlpha = new float[255];
		public static float[] MSBarAlpha = new float[255];
		static Vector2 LastDrawPosition = Vector2.Zero;
		public static void DrawHealthBar(On.Terraria.Main.orig_DrawInterface_14_EntityHealthBars DrawInterface_14_EntityHealthBars, Main instance) {
			if (Main.netMode != 2 && !Main.gameMenu && !Main.ingameOptionsWindow) {
				for (int i = 0; i < 255; i++) {
					BarOffset[i] = 0f;
					Player player = Main.player[i];
					if (player.active && (player.whoAmI == Main.myPlayer || (player.team > 0 && player.team == Main.player[Main.myPlayer].team))) {
						Vector2 DrawPosition = new Vector2((int)player.Center.X - Main.screenPosition.X, (int)player.Bottom.Y - Main.screenPosition.Y);
						if (player.whoAmI != Main.myPlayer && Main.player[Main.myPlayer].gravDir == -1f) {
							DrawPosition.Y = Main.screenPosition.Y + Main.screenHeight - player.position.Y;
						}
						if (Config.Client.ShowPlayerHealthBars) {
							if (player.statLife == player.statLifeMax2 || player.dead) { HBarAlpha[i] -= Config.Client.FadeOutAmount; }
							else { HBarAlpha[i] += Config.Client.FadeInAmount; }
							HBarAlpha[i] = MathHelper.Clamp(HBarAlpha[i], 0, 1);
							if (HBarAlpha[i] > 0) {
								float Percentage = MathHelper.Clamp((float)player.statLife / (float)player.statLifeMax2, 0, 1);
								float R = MathHelper.Clamp((1f - Percentage) * 2f, 0, 1);
								float G = MathHelper.Clamp(Percentage * 2f, 0, 1);
								float B = 0f;
								Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
								DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), HBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
								if (Config.Client.ShowIcons) {
									Main.spriteBatch.Draw(Textures.Heart, NewPosition, null, Color.White * HBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.Flame.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
								}
								if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
									Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * HBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
								}
								BarOffset[i] += HBarAlpha[i];
							}
						}
						if (Config.Client.ShowPlayerManaBars) {
							if (player.statMana == player.statManaMax2 || player.dead) { MBarAlpha[i] -= Config.Client.FadeOutAmount; }
							else { MBarAlpha[i] += Config.Client.FadeInAmount; }
							MBarAlpha[i] = MathHelper.Clamp(MBarAlpha[i], 0, 1);
							if (MBarAlpha[i] > 0) {
								float Percentage = MathHelper.Clamp((float)player.statMana / (float)player.statManaMax2, 0, 1);
								float R = MathHelper.Clamp((1f - Percentage), 0.25f, 0.5f);
								float G = MathHelper.Clamp((1f - Percentage), 0.25f, 0.5f);
								float B = MathHelper.Clamp(Percentage, 0.5f, 1f);
								Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
								DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), MBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
								if (Config.Client.ShowIcons) {
									Main.spriteBatch.Draw(Textures.Mana, NewPosition, null, Color.White * MBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.Mana.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
								}
								if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
									Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * MBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
								}
								BarOffset[i] += MBarAlpha[i];
							}
						}
						if (PlayerHealthManaBar.PowerAttacksLoaded && Import.PA_ShowChargeBar) {
							DoPowerAttacksBars(i, player, DrawPosition);
						}
						if (Config.Client.ShowPlayerOtherResourceBars) {
							{
								if (player.breath == player.breathMax || player.dead) { BBarAlpha[i] -= Config.Client.FadeOutAmount; }
								else { BBarAlpha[i] += Config.Client.FadeInAmount; }
								BBarAlpha[i] = MathHelper.Clamp(BBarAlpha[i], 0, 1);
								if (BBarAlpha[i] > 0) {
									float Percentage = MathHelper.Clamp((float)player.breath / (float)player.breathMax, 0, 1);
									float R = MathHelper.Clamp(Percentage, 0.25f, 0.75f);
									float G = MathHelper.Clamp(Percentage, 0.25f, 0.75f);
									float B = MathHelper.Clamp(Percentage, 0.5f, 1f);
									Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
									DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), BBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
									if (Config.Client.ShowIcons) {
										Main.spriteBatch.Draw(Textures.Bubble, NewPosition, null, Color.White * BBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.Bubble.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
									}
									if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
										Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * BBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
									}
									BarOffset[i] += BBarAlpha[i];
								}
							}
							{
								if (player.lavaTime == player.lavaMax || player.dead) { LBarAlpha[i] -= Config.Client.FadeOutAmount; }
								else { LBarAlpha[i] += Config.Client.FadeInAmount; }
								LBarAlpha[i] = MathHelper.Clamp(LBarAlpha[i], 0, 1);
								if (LBarAlpha[i] > 0) {
									float Percentage = MathHelper.Clamp((float)player.lavaTime / (float)player.lavaMax, 0, 1);
									float R = MathHelper.Clamp(Percentage * 2f, 0.5f, 1f);
									float G = MathHelper.Clamp(Percentage, 0.25f, 0.75f);
									float B = 0f;
									Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
									DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), LBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
									if (Config.Client.ShowIcons) {
										Main.spriteBatch.Draw(Textures.Flame, NewPosition, null, Color.White * LBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.Flame.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
									}
									if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
										Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * LBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
									}
									BarOffset[i] += LBarAlpha[i];
								}
							}
						}
						if (Config.Client.ShowPotionSicknessBars) {
							if (Item.potionDelay > 0) {
								int amount = 0;
								for (int j = 0; j < player.buffType.Count(); j++) {
									if (player.buffType[j] == BuffID.PotionSickness) {
										amount = player.buffTime[j];
									}
								}
								
								if (amount <= 0 || player.dead) { PSBarAlpha[i] -= Config.Client.FadeOutAmount; }
								else { PSBarAlpha[i] += Config.Client.FadeInAmount; }
								PSBarAlpha[i] = MathHelper.Clamp(PSBarAlpha[i], 0, 1);
								if (PSBarAlpha[i] > 0) {
									float Percentage = MathHelper.Clamp((float)amount / (float)Item.potionDelay, 0, 1);
									float R = MathHelper.Clamp(Percentage * 2, 0.5f, 1f);
									float G = 0.5f;
									float B = 0.5f;
									Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
									DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), PSBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
									if (Config.Client.ShowIcons) {
										Main.spriteBatch.Draw(Textures.PotionSick, NewPosition, null, Color.White * PSBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PotionSick.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
									}
									if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
										Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * PSBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
									}
									BarOffset[i] += PSBarAlpha[i];
								}
							}
							if (Player.manaSickTimeMax > 0) {
								int amount = 0;
								for (int j = 0; j < player.buffType.Count(); j++) {
									if (player.buffType[j] == BuffID.ManaSickness) {
										amount = player.buffTime[j];
									}
								}
								
								if (amount <= 0 || player.dead) { MSBarAlpha[i] -= Config.Client.FadeOutAmount; }
								else { MSBarAlpha[i] += Config.Client.FadeInAmount; }
								MSBarAlpha[i] = MathHelper.Clamp(MSBarAlpha[i], 0, 1);
								if (MSBarAlpha[i] > 0) {
									float Percentage = MathHelper.Clamp((float)amount / (float)Player.manaSickTimeMax, 0, 1);
									float R = 0.5f;
									float G = 0.5f;
									float B = MathHelper.Clamp(Percentage * 2, 0.5f, 1f);
									Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
									DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), MSBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
									if (Config.Client.ShowIcons) {
										Main.spriteBatch.Draw(Textures.ManaSick, NewPosition, null, Color.White * MSBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.ManaSick.Width / 2), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
									}
									if (Config.Client.ShowWarningIcons && Percentage < Config.Client.WarningThreshold && DALib.DALib.tick % 30 < 15) {
										Main.spriteBatch.Draw(Textures.Exclimation, NewPosition, null, Color.White * MSBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(-(Textures.HealthBar1.Width - 18), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);	
									}
									BarOffset[i] += MSBarAlpha[i];
								}
							}
						}
					}
				}
				if (Config.Client.ShowNPCHealthBars) {
					for (int i=0; i < 200; i++) {
						NPC npc = Main.npc[i];
						Player player = Main.player[Main.myPlayer];
						if (npc.active && npc.realLife >= 0) { npc = Main.npc[npc.realLife]; }
						if (npc.active) {
							Vector2 DrawPosition = new Vector2((int)npc.Center.X - Main.screenPosition.X, (int)npc.Bottom.Y - Main.screenPosition.Y);
							if (Main.player[Main.myPlayer].gravDir == -1f) {
								DrawPosition.Y = Main.screenPosition.Y + Main.screenHeight - npc.position.Y;
							}
							Texture2D HealthBar1 = Textures.HealthBar1;
							Texture2D HealthBar2 = Textures.HealthBar2;
							Texture2D Heart = Textures.Heart;
							if (npc.boss) {
								HealthBar1 = Textures.BigHealthBar1;
								HealthBar2 = Textures.BigHealthBar2;
								Heart = Textures.BigHeart;
							}
							Color LColor = Lighting.GetColor((int)(npc.Center.X / 16), (int)(npc.Center.Y / 16)) * (npc.boss ? 8 : 4);
							/*if (npc.realLife >= 0) {
								NPC parent = Main.npc[npc.realLife];
								Color PLColor = Lighting.GetColor((int)(parent.Center.X / 16), (int)(parent.Center.Y / 16));
								byte Brightest = (byte)Math.Max(PLColor.R, Math.Max(PLColor.G, PLColor.B));
								//List<float> X = new List<float>{ parent.Center.X };
								//List<float> Y = new List<float>{ parent.Bottom.Y };
								bool anyVisible = false;
								int z = 0;
								while (parent.ai[0] > 0 && z++ < 200) {
									if (Math.Abs(parent.Center.X - Main.player[Main.myPlayer].Center.X) < Main.screenWidth || Math.Abs(parent.Center.Y - Main.player[Main.myPlayer].Center.Y) < Main.screenHeight) {
										anyVisible = true;
										PLColor = Lighting.GetColor((int)(parent.Center.X / 16), (int)(parent.Center.Y / 16));
										Brightest = (byte)Math.Max(Brightest, Math.Max(PLColor.R, Math.Max(PLColor.G, PLColor.B)));
									}
									parent = Main.npc[(int)parent.ai[0]];
								//	X.Add(parent.Center.X);
								//	Y.Add(parent.Bottom.Y);
								}
								//DrawPosition = new Vector2((int)X.Average() - Main.screenPosition.X, (int)Y.Average() - Main.screenPosition.Y);
								if (anyVisible) {
									LColor = new Color(Brightest, Brightest, Brightest, 255) * 4;
									int width = HealthBar1.Width;
									int height = HealthBar1.Height * 2;
									if (DrawPosition.X < width) {
										DrawPosition.X = width;
									}
									if (DrawPosition.X > Main.screenWidth - width) {
										DrawPosition.X = Main.screenWidth - width;
									}
									if (DrawPosition.Y < 0) {
										DrawPosition.Y = height;
									}
									if (DrawPosition.Y > Main.screenHeight - height) {
										DrawPosition.Y = Main.screenHeight - height;
									}
								}
							}*/
							if (npc.realLife >= 0) {
								NPC parent = Main.npc[npc.realLife];
								NPC closest = parent;
								int z = 0;
								while (parent.ai[0] > 0 && z++ < 200) {
									parent = Main.npc[(int)parent.ai[0]];
									if (Vector2.Distance(parent.Center, player.Center) < Vector2.Distance(closest.Center, player.Center)) {
										closest = parent;
									}
								}
								LColor = Lighting.GetColor((int)(closest.Center.X / 16), (int)(closest.Center.Y / 16)) * 4;
								Vector2 NewDrawPosition = new Vector2((int)closest.Center.X - Main.screenPosition.X, (int)closest.Bottom.Y - Main.screenPosition.Y);
								if (LastDrawPosition == Vector2.Zero || Vector2.Distance(LastDrawPosition, NewDrawPosition) > Math.Max(npc.width, npc.height) * 5) {
									DrawPosition = NewDrawPosition;
								}
								else {
									DrawPosition = Lerp(LastDrawPosition, NewDrawPosition, (1 / 600f) * Math.Max(Math.Abs(player.velocity.X), Math.Abs(player.velocity.Y)));
								}
								LastDrawPosition = DrawPosition;
							}
							{
								if (npc.life == npc.lifeMax || npc.life <= 0) { NPCHBarAlpha[i] -= Config.Client.FadeOutAmount; }
								else { NPCHBarAlpha[i] += Config.Client.FadeInAmount; }
								NPCHBarAlpha[i] = MathHelper.Clamp(NPCHBarAlpha[i], 0, 1);
								if (NPCHBarAlpha[i] > 0) {
									float Percentage = MathHelper.Clamp((float)npc.life / (float)npc.lifeMax, 0, 1);
									float R = MathHelper.Clamp((1f - Percentage) * 2f, 0, 1);
									float G = MathHelper.Clamp(Percentage * 2f, 0, 1);
									float B = 0f;
									float ColorMod = Math.Max(LColor.R, Math.Max(LColor.G, LColor.B)) / 255f;
									Color BarColor = new Color(R * ColorMod, G * ColorMod, B * ColorMod);
									Color IconColor = new Color(1f * ColorMod, 1f * ColorMod, 1f * ColorMod);
									Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f));
									DALib.UI.DrawHealthBar(NewPosition, Percentage, BarColor, NPCHBarAlpha[i] * Config.Client.MaxAlpha, 1f, HealthBar1, HealthBar2);
									if (Config.Client.ShowIcons) {
										Main.spriteBatch.Draw(Heart, NewPosition, null, IconColor * NPCHBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(HealthBar1.Width - (Heart.Width / 2), HealthBar1.Height), 1f, SpriteEffects.None, 0f);
									}
								}
							}
						}
					}
				}
			}
			return;
		}
		public static float[] PA_CBarAlpha = new float[255];
		public static void DoPowerAttacksBars(int i, Player player, Vector2 DrawPosition) {
			if (PlayerHealthManaBar.PowerAttacksLoaded && Import.PA_ShowChargeBar) {
				if (Import.PA_ChargedAmount(player) <= 0 || player.dead) { PA_CBarAlpha[i] -= Config.Client.FadeOutAmount; }
				else { PA_CBarAlpha[i] += Config.Client.FadeInAmount; }
				PA_CBarAlpha[i] = MathHelper.Clamp(PA_CBarAlpha[i], 0, 1);
				if (PA_CBarAlpha[i] > 0) {
					float Percentage = MathHelper.Clamp((float)Import.PA_ChargedAmount(player) / (float)Import.PA_MaxChargeAmount(player), 0, 1);
					float R = MathHelper.Clamp(Percentage * 2f, 0, 1);
					float G = MathHelper.Clamp((1f - Percentage) * 2f, 0, 1);
					float B = 0f;
					Vector2 NewPosition = new Vector2(DrawPosition.X, DrawPosition.Y + (Textures.HealthBar1.Height * 1.5f) + ((Textures.HealthBar1.Height + Config.Client.BarSpacing) * BarOffset[i]));
					DALib.UI.DrawHealthBar(NewPosition, Percentage, new Color(R, G, B), PA_CBarAlpha[i] * Config.Client.MaxAlpha, 1f, Textures.HealthBar1, Textures.HealthBar2);
					if (Config.Client.ShowIcons) {
						int icons = 0;
						/*if (Import.PA_BoostedCritical(player)) {
							Main.spriteBatch.Draw(Textures.PA_Critical, NewPosition, null, new Color(R, G, B) * PA_CBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PA_Critical.Width / 2) + (Textures.PA_Critical.Width * icons++), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
						}
						if (Import.PA_BoostedVelocity(player)) {
							Main.spriteBatch.Draw(Textures.PA_Velocity, NewPosition, null, new Color(R, G, B) * PA_CBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PA_Velocity.Width / 2) + (Textures.PA_Velocity.Width * icons++), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
						}
						if (Import.PA_BoostedDamage(player)) {
							Main.spriteBatch.Draw(Textures.PA_Damage, NewPosition, null, new Color(R, G, B) * PA_CBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PA_Damage.Width / 2) + (Textures.PA_Damage.Width * icons++), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
						}
						if (Import.PA_MustFullCharge(player)) {
							Main.spriteBatch.Draw(Textures.PA_FullCharge, NewPosition, null, new Color(R, G, B) * PA_CBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PA_FullCharge.Width / 2) + (Textures.PA_FullCharge.Width * icons++), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
						}*/
						if (icons == 0) {
							Main.spriteBatch.Draw(Textures.PA_Power, NewPosition, null, new Color(R, G, B) * PA_CBarAlpha[i] * Config.Client.MaxAlpha, 0f, new Vector2(Textures.HealthBar1.Width - (Textures.PA_Power.Width / 2) + (Textures.PA_Power.Width * icons++), Textures.HealthBar1.Height), 1f, SpriteEffects.None, 0f);
						}
					}
					
					BarOffset[i] += PA_CBarAlpha[i];
				}
			}
		}
		
		static float Lerp(float firstFloat, float secondFloat, float by) {
			 return firstFloat * (1 - by) + secondFloat * by;
		}
		
		static Vector2 Lerp(Vector2 firstVector, Vector2 secondVector, float by) {
			float retX = Lerp(firstVector.X, secondVector.X, by);
			float retY = Lerp(firstVector.Y, secondVector.Y, by);
			return new Vector2(retX, retY);
		}
	}
	
	public static class Import {
		public static bool PA_ShowChargeBar {
			get {
				if (PlayerHealthManaBar.PowerAttacksLoaded) {
					return PowerAttacks.Config.Client.ShowChargeBar;
				}
				return false;
			}
			set {
				if (PlayerHealthManaBar.PowerAttacksLoaded) {
					PowerAttacks.Config.Client.ShowChargeBar = value;
				}
			}
		}
		
		public static bool PA_BoostedCritical(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				Item item = DALib.Functions.GetHeldItem(player);
				if (item.type != 0) {
					PowerAttacks.GItem itemInfo = item.GetGlobalItem<PowerAttacks.GItem>(ModLoader.GetMod("PowerAttacks"));
					if (itemInfo != null) {
						return itemInfo.BoostedCritical;
					}
				}
			}
			return false;
		}
		
		public static bool PA_BoostedDamage(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				Item item = DALib.Functions.GetHeldItem(player);
				if (item.type != 0) {
					PowerAttacks.GItem itemInfo = item.GetGlobalItem<PowerAttacks.GItem>(ModLoader.GetMod("PowerAttacks"));
					if (itemInfo != null) {
						return itemInfo.BoostedDamage;
					}
				}
			}
			return false;
		}
		
		public static bool PA_BoostedVelocity(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				Item item = DALib.Functions.GetHeldItem(player);
				if (item.type != 0) {
					PowerAttacks.GItem itemInfo = item.GetGlobalItem<PowerAttacks.GItem>(ModLoader.GetMod("PowerAttacks"));
					if (itemInfo != null) {
						return itemInfo.BoostedVelocity;
					}
				}
			}
			return false;
		}
		
		public static bool PA_MustFullCharge(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				Item item = DALib.Functions.GetHeldItem(player);
				if (item.type != 0) {
					PowerAttacks.GItem itemInfo = item.GetGlobalItem<PowerAttacks.GItem>(ModLoader.GetMod("PowerAttacks"));
					if (itemInfo != null) {
						return itemInfo.MustFullCharge;
					}
				}
			}
			return false;
		}
		
		public static float PA_ChargedAmount(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				PowerAttacks.MPlayer playerInfo = player.GetModPlayer<PowerAttacks.MPlayer>(ModLoader.GetMod("PowerAttacks"));
				if (playerInfo != null) {
					return playerInfo.ChargedAmount;
				}
			}
			return 0f;
		}
		
		public static float PA_MaxChargeAmount(Player player) {
			if (PlayerHealthManaBar.PowerAttacksLoaded) {
				Item item = DALib.Functions.GetHeldItem(player);
				if (item.type != 0) {
					PowerAttacks.GItem itemInfo = item.GetGlobalItem<PowerAttacks.GItem>(ModLoader.GetMod("PowerAttacks"));
					if (itemInfo != null && itemInfo.IsChargeable) {
						return itemInfo.GetMaxChargeAmount(item);
					}
				}
			}
			return 0f;
		}
	}

	public class MPlayer : ModPlayer {
		public override void PreUpdate() {
			if (player.whoAmI != Main.myPlayer) { return; }
			if (Main.netMode == 1 && !Main.gameMenu) {
				ModPacket netMessage = mod.GetPacket();
				netMessage.Write(Config.modPrefix);
				netMessage.Write((int)player.whoAmI);
				netMessage.Write((int)player.breath);
				netMessage.Write((int)player.breathMax);
				netMessage.Write((int)player.lavaTime);
				netMessage.Write((int)player.lavaMax);
				netMessage.Send();
			}
		}
	}
}